﻿namespace GRINGlobal.Client.Common.Genesys
{
    partial class UploadData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UploadData));
            this.btnUpload = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lstFields = new System.Windows.Forms.ListBox();
            this.dgAccessions = new System.Windows.Forms.DataGridView();
            this.lblFields = new System.Windows.Forms.Label();
            this.lblRecords = new System.Windows.Forms.Label();
            this.ssStatusStrip = new System.Windows.Forms.StatusStrip();
            this.lblRecordCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblErrorCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblUploading = new System.Windows.Forms.ToolStripStatusLabel();
            this.pbProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.pnlDataElements = new System.Windows.Forms.Panel();
            this.gbPassportElements = new System.Windows.Forms.GroupBox();
            this.lblMissingElements = new System.Windows.Forms.Label();
            this.lstMissingFields = new System.Windows.Forms.ListBox();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.ACCENUMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgAccessions)).BeginInit();
            this.ssStatusStrip.SuspendLayout();
            this.pnlDataElements.SuspendLayout();
            this.gbPassportElements.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnUpload
            // 
            this.btnUpload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpload.Location = new System.Drawing.Point(379, 413);
            this.btnUpload.Margin = new System.Windows.Forms.Padding(4);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(100, 28);
            this.btnUpload.TabIndex = 0;
            this.btnUpload.Text = "&Upload";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(487, 413);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Close";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lstFields
            // 
            this.lstFields.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lstFields.ItemHeight = 16;
            this.lstFields.Location = new System.Drawing.Point(18, 46);
            this.lstFields.Name = "lstFields";
            this.lstFields.Size = new System.Drawing.Size(161, 164);
            this.lstFields.TabIndex = 2;
            // 
            // dgAccessions
            // 
            this.dgAccessions.AllowUserToAddRows = false;
            this.dgAccessions.AllowUserToDeleteRows = false;
            this.dgAccessions.AllowUserToResizeRows = false;
            this.dgAccessions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgAccessions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAccessions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ACCENUMB,
            this.STATUS,
            this.ID});
            this.dgAccessions.Location = new System.Drawing.Point(21, 29);
            this.dgAccessions.MultiSelect = false;
            this.dgAccessions.Name = "dgAccessions";
            this.dgAccessions.ReadOnly = true;
            this.dgAccessions.RowHeadersVisible = false;
            this.dgAccessions.RowTemplate.Height = 24;
            this.dgAccessions.ShowCellErrors = false;
            this.dgAccessions.ShowCellToolTips = false;
            this.dgAccessions.ShowEditingIcon = false;
            this.dgAccessions.ShowRowErrors = false;
            this.dgAccessions.Size = new System.Drawing.Size(565, 368);
            this.dgAccessions.TabIndex = 3;
            // 
            // lblFields
            // 
            this.lblFields.AutoSize = true;
            this.lblFields.Location = new System.Drawing.Point(15, 26);
            this.lblFields.Name = "lblFields";
            this.lblFields.Size = new System.Drawing.Size(65, 17);
            this.lblFields.TabIndex = 5;
            this.lblFields.Text = "Included:";
            // 
            // lblRecords
            // 
            this.lblRecords.AutoSize = true;
            this.lblRecords.Location = new System.Drawing.Point(18, 9);
            this.lblRecords.Name = "lblRecords";
            this.lblRecords.Size = new System.Drawing.Size(65, 17);
            this.lblRecords.TabIndex = 6;
            this.lblRecords.Text = "Records:";
            // 
            // ssStatusStrip
            // 
            this.ssStatusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ssStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblRecordCount,
            this.lblErrorCount,
            this.lblUploading,
            this.pbProgress});
            this.ssStatusStrip.Location = new System.Drawing.Point(0, 455);
            this.ssStatusStrip.Name = "ssStatusStrip";
            this.ssStatusStrip.Size = new System.Drawing.Size(822, 25);
            this.ssStatusStrip.TabIndex = 7;
            this.ssStatusStrip.Text = "statusStrip1";
            // 
            // lblRecordCount
            // 
            this.lblRecordCount.Name = "lblRecordCount";
            this.lblRecordCount.Size = new System.Drawing.Size(77, 20);
            this.lblRecordCount.Text = "Records: 0";
            // 
            // lblErrorCount
            // 
            this.lblErrorCount.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblErrorCount.Margin = new System.Windows.Forms.Padding(50, 3, 0, 2);
            this.lblErrorCount.Name = "lblErrorCount";
            this.lblErrorCount.Size = new System.Drawing.Size(62, 20);
            this.lblErrorCount.Text = "Errors: 0";
            this.lblErrorCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUploading
            // 
            this.lblUploading.AutoSize = false;
            this.lblUploading.Margin = new System.Windows.Forms.Padding(50, 3, 0, 2);
            this.lblUploading.Name = "lblUploading";
            this.lblUploading.Size = new System.Drawing.Size(118, 20);
            this.lblUploading.Text = "Uploading Data:";
            this.lblUploading.Visible = false;
            // 
            // pbProgress
            // 
            this.pbProgress.AutoSize = false;
            this.pbProgress.Name = "pbProgress";
            this.pbProgress.Size = new System.Drawing.Size(180, 19);
            this.pbProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.pbProgress.Visible = false;
            // 
            // pnlDataElements
            // 
            this.pnlDataElements.Controls.Add(this.gbPassportElements);
            this.pnlDataElements.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlDataElements.Location = new System.Drawing.Point(0, 0);
            this.pnlDataElements.Name = "pnlDataElements";
            this.pnlDataElements.Size = new System.Drawing.Size(212, 455);
            this.pnlDataElements.TabIndex = 8;
            // 
            // gbPassportElements
            // 
            this.gbPassportElements.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbPassportElements.Controls.Add(this.lblFields);
            this.gbPassportElements.Controls.Add(this.lstFields);
            this.gbPassportElements.Controls.Add(this.lblMissingElements);
            this.gbPassportElements.Controls.Add(this.lstMissingFields);
            this.gbPassportElements.Location = new System.Drawing.Point(12, 9);
            this.gbPassportElements.Name = "gbPassportElements";
            this.gbPassportElements.Size = new System.Drawing.Size(194, 432);
            this.gbPassportElements.TabIndex = 8;
            this.gbPassportElements.TabStop = false;
            this.gbPassportElements.Text = "Passport Elements";
            // 
            // lblMissingElements
            // 
            this.lblMissingElements.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblMissingElements.AutoSize = true;
            this.lblMissingElements.Location = new System.Drawing.Point(15, 226);
            this.lblMissingElements.Name = "lblMissingElements";
            this.lblMissingElements.Size = new System.Drawing.Size(59, 17);
            this.lblMissingElements.TabIndex = 6;
            this.lblMissingElements.Text = "Missing:";
            // 
            // lstMissingFields
            // 
            this.lstMissingFields.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lstMissingFields.FormattingEnabled = true;
            this.lstMissingFields.ItemHeight = 16;
            this.lstMissingFields.Location = new System.Drawing.Point(18, 249);
            this.lstMissingFields.Name = "lstMissingFields";
            this.lstMissingFields.Size = new System.Drawing.Size(161, 164);
            this.lstMissingFields.TabIndex = 7;
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.btnUpload);
            this.pnlMain.Controls.Add(this.btnCancel);
            this.pnlMain.Controls.Add(this.lblRecords);
            this.pnlMain.Controls.Add(this.dgAccessions);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(212, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(610, 455);
            this.pnlMain.TabIndex = 9;
            // 
            // ACCENUMB
            // 
            this.ACCENUMB.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ACCENUMB.DataPropertyName = "ACCENUMB";
            this.ACCENUMB.FillWeight = 40F;
            this.ACCENUMB.HeaderText = "Accession Number";
            this.ACCENUMB.Name = "ACCENUMB";
            this.ACCENUMB.ReadOnly = true;
            // 
            // STATUS
            // 
            this.STATUS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.STATUS.DataPropertyName = "STATUS";
            this.STATUS.FillWeight = 60F;
            this.STATUS.HeaderText = "Status";
            this.STATUS.Name = "STATUS";
            this.STATUS.ReadOnly = true;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // UploadData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(822, 480);
            this.ControlBox = false;
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlDataElements);
            this.Controls.Add(this.ssStatusStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(710, 450);
            this.Name = "UploadData";
            this.Text = "Upload Accession Data";
            this.Load += new System.EventHandler(this.UploadData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgAccessions)).EndInit();
            this.ssStatusStrip.ResumeLayout(false);
            this.ssStatusStrip.PerformLayout();
            this.pnlDataElements.ResumeLayout(false);
            this.gbPassportElements.ResumeLayout(false);
            this.gbPassportElements.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ListBox lstFields;
        private System.Windows.Forms.DataGridView dgAccessions;
        private System.Windows.Forms.Label lblFields;
        private System.Windows.Forms.Label lblRecords;
        private System.Windows.Forms.StatusStrip ssStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lblRecordCount;
        private System.Windows.Forms.ToolStripStatusLabel lblUploading;
        private System.Windows.Forms.ToolStripProgressBar pbProgress;
        private System.Windows.Forms.ToolStripStatusLabel lblErrorCount;
        private System.Windows.Forms.Panel pnlDataElements;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.ListBox lstMissingFields;
        private System.Windows.Forms.Label lblMissingElements;
        private System.Windows.Forms.GroupBox gbPassportElements;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACCENUMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}
﻿using GeneSys2.Client;
using GeneSys2.Client.Model;
using GeneSys2.Client.OAuth;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GRINGlobal.Client.Common.Genesys
{
    /// <summary>
    /// Gensys Connection Configuration Wizard. Sets up and validates
    /// the values used to access Gensys.
    /// </summary>
    public partial class ConfigWizard : Form
    {
        /// <summary>
        /// Enumeration representing the panels used in the wizard.
        /// </summary>
        private enum PanelType
        {
            Welcome = 0,
            ClientInfo = 1,
            VerificationCode = 2,
            ConfigSuccess = 3
        }

        private List<Panel> _panels = new List<Panel>();
        private PanelType _currentPanel;
        ISyncController _controller;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="controller">Reference to the Sync Controller orchestrating the Genesys connection</param>
        public ConfigWizard(ISyncController controller)
        {
            this._controller = controller;

            InitializeComponent();
        }

        /// <summary>
        /// Form load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfigWizard_Load(object sender, EventArgs e)
        {
            // Setup UI elements
            _panels.Add(pnlWelcome);
            _panels.Add(pnlClientInfo);
            _panels.Add(pnlVerificationCode);
            _panels.Add(pnlConfigSuccess);
            btnPrevious.Enabled = false;
        }

        /// <summary>
        /// Next button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnNext_Click(object sender, EventArgs e)
        {
            // Set the next panel based on the current panel. Verify that
            // we can advance prior to changing the panel.
            switch (_currentPanel)
            {
                case PanelType.Welcome:
                    SetPanel(PanelType.ClientInfo);
                    break;
                case PanelType.ClientInfo:
                    if (CheckServerAddressValid())
                    {
                        Uri serverAddr = new Uri(txtServerAddress.Text);

                        // Set client key/secret in the token store
                        _controller.UpdateClientID(serverAddr, txtClientKey.Text, txtClientSecret.Text);

                        switch (_controller.CheckConnectivity())
                        {
                            case ConnectivityTestResult.Success:
                                SetPanel(PanelType.ConfigSuccess);
                                break;
                            case ConnectivityTestResult.Unauthorized:
                                SetPanel(PanelType.VerificationCode);
                                break;
                            case ConnectivityTestResult.ServiceUnavailable:
                                MessageBox.Show(GenesysResources.ServiceUnavailable_Message, GenesysResources.ServiceUnavailable_Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                break;
                            case ConnectivityTestResult.NoConnection:
                                MessageBox.Show(string.Format(GenesysResources.NoConnection_Message, txtServerAddress.Text), GenesysResources.NoConnection_Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                break;
                        }
                    }
                    else
                    {
                        MessageBox.Show(string.Format(GenesysResources.InvalidServerAddress, txtServerAddress.Text), GenesysResources.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;
                case PanelType.VerificationCode:
                    // Check whether the verification code is valid. If so, advance to the success panel.
                    bool tokensSet = await _controller.AuthProvider.GetTokensAsync(txtVerificationCode.Text);
                    
                    if (tokensSet && _controller.CheckConnectivity() == ConnectivityTestResult.Success)
                        SetPanel(PanelType.ConfigSuccess);
                    else
                        MessageBox.Show(GenesysResources.BadVerificationCode_Message, GenesysResources.BadVerificationCode_Caption, MessageBoxButtons.OK, MessageBoxIcon.Error);

                    break;
                case PanelType.ConfigSuccess:
                    // All done. Close the wizard
                    this.Close();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Previous button click.  Change to prior panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrevious_Click(object sender, EventArgs e)
        {
            switch (_currentPanel)
            {
                case PanelType.Welcome:
                    break;
                case PanelType.ClientInfo:
                    SetPanel(PanelType.Welcome);
                    break;
                case PanelType.VerificationCode:
                    SetPanel(PanelType.ClientInfo);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Get verification code button clicked. Open a browser window to correct page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGenVerificationCode_Click(object sender, EventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo(_controller.AuthProvider.VerificationCodeRequestUrl);
            Process.Start(sInfo);
        }

        /// <summary>
        /// Client info panel loads the current key and secret
        /// when it becomes visible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlClientInfo_VisibleChanged(object sender, EventArgs e)
        {
            if(pnlClientInfo.Visible)
            {
                txtServerAddress.Text = _controller.TokenStorage.ServerAddress.AbsoluteUri;
                txtClientKey.Text = _controller.TokenStorage.ClientKey;
                txtClientSecret.Text = _controller.TokenStorage.ClientSecret;
            }
        }

        // Common event handler to update the Next button state on control changes.
        private void common_CheckNextButtonState(object sender, EventArgs e)
        {
            UpdateNextButtonState();
        }

        /// <summary>
        /// Set the current panel, adjusting the button states as 
        /// needed.
        /// </summary>
        /// <param name="type"></param>
        private void SetPanel(PanelType type)
        {
            // Set visibility for the new panel
            foreach (Panel p in _panels)
                p.Visible = false;
            _panels[(int)type].Visible = true;
            _currentPanel = type;

            // Update button states
            UpdateNextButtonState();
            btnPrevious.Enabled = _currentPanel != PanelType.Welcome;
            btnPrevious.Visible = _currentPanel != PanelType.ConfigSuccess;
        }

        /// <summary>
        /// Updates the state of the next button based on the current
        /// panel and user input.
        /// </summary>
        private void UpdateNextButtonState()
        {
            switch (_currentPanel)
            {
                case PanelType.Welcome:
                    btnNext.Enabled = true;
                    break;
                case PanelType.ClientInfo:
                    btnNext.Enabled = (txtServerAddress.TextLength > 0 && txtClientKey.TextLength > 0 && txtClientSecret.TextLength > 0);
                    break;
                case PanelType.VerificationCode:
                    btnNext.Enabled = txtVerificationCode.TextLength > 0;
                    break;
                case PanelType.ConfigSuccess:
                    btnNext.Text = "&Close";
                    btnNext.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Checks whether the server url format is valid
        /// </summary>
        /// <returns></returns>
        private bool CheckServerAddressValid()
        {
            bool valid = Uri.IsWellFormedUriString(txtServerAddress.Text, UriKind.Absolute);

            if (valid)
            {
                // Ensure either HTTP or HTTPS
                var parsed = new UriBuilder(txtServerAddress.Text);

                if (string.Compare(parsed.Scheme, "http", true) != 0 && string.Compare(parsed.Scheme, "https", true) != 0)
                {
                    parsed.Scheme = Uri.UriSchemeHttps;
                    txtServerAddress.Text = parsed.Uri.AbsoluteUri;
                }
            }

            return valid;
        }
    }
}

﻿using GeneSys2.Client;
using GeneSys2.Client.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GRINGlobal.Client.Common.Genesys
{
    /// <summary>
    /// Upload data form commits accession information to the Genesys system 
    /// </summary>
    public partial class UploadData : Form
    {
        /// <summary>
        /// Maximum number of records to commit to Genesys at once.
        /// </summary>
        const int BATCH_SIZE = 50;

        private ISyncController _controller;
        private DataTable _data = null;
        private List<string> _fieldsToSync = null;
        private List<string> _fieldsMissing = null;
        private DataTable _statusData = null;
        private SyncType _mode;
        private BackgroundWorker _bw = null;
        Cursor _origCursor = null;

        /// <summary>
        /// Form constructor
        /// </summary>
        /// <param name="controller">Sync Controller orchestrating the data transfer</param>
        /// <param name="data">Data to upload</param>
        /// <param name="mode">Upsert or delete</param>
        public UploadData(ISyncController controller, DataTable data, SyncType mode)
        {
            _controller = controller;
            _data = data;
            _fieldsToSync = new List<string>();
            _fieldsMissing = new List<string>();
            _mode = mode;

            // Setup upload data feedback grid
            _statusData = new DataTable();
            var column = new DataColumn("ACCENUMB");
            column.Caption = "Accession Number";
            _statusData.Columns.Add(column);
            _statusData.Columns.Add("STATUS");
            _statusData.Columns.Add("ID");

            // Configure background worker
            _bw = new BackgroundWorker();
            _bw.WorkerReportsProgress = true;
            _bw.RunWorkerCompleted += UploadRunWorkerCompleted;
            _bw.ProgressChanged += UploadProgressChanged;
            if (this._mode == SyncType.Upsert)
                _bw.DoWork += UploadAccessionData;
            else
                _bw.DoWork += DeleteAccessionData;

            InitializeComponent();
        }

        /// <summary>
        /// Load event prepares form UI elements
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadData_Load(object sender, EventArgs e)
        {
            pbProgress.Style = ProgressBarStyle.Marquee;

            dgAccessions.DataSource = _statusData;
            pnlDataElements.Visible = (_mode == SyncType.Upsert);
            if (_mode == SyncType.Delete)
            {
                pnlDataElements.Visible = false;
                btnUpload.Text = "&Delete";
                this.Text = GenesysResources.Delete_Form_Title;
            }

            InterrogateData();
        }

        /// <summary>
        /// Upload or Delete button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (_bw.IsBusy)
                return;

            btnCancel.Enabled = false;
            btnUpload.Enabled = false;
            lblUploading.Visible = true;
            pbProgress.Visible = true;

            lblRecordCount.Text = string.Format(GenesysResources.Records, 0, _data.DefaultView.Count);
            lblErrorCount.Text = string.Format(GenesysResources.Errors, 0);
            lblErrorCount.ForeColor = SystemColors.ControlText;

            // Change cursor to the wait cursor...
            _origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            _bw.RunWorkerAsync();
        }

        /// <summary>
        /// Update progress of upload
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState is UploadStatus)
            {
                UploadStatus status = (UploadStatus)e.UserState;

                lblRecordCount.Text = string.Format(GenesysResources.Records, status.Processed, status.Total);
                lblErrorCount.Text = string.Format(GenesysResources.Errors, status.Errors);
                if (status.Errors > 0)
                    lblErrorCount.ForeColor = Color.Red;
            }
            Cursor.Current = Cursors.WaitCursor;
        }

        /// <summary>
        /// Upload thread has completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnCancel.Enabled = true;
            btnUpload.Enabled = true;
            lblUploading.Visible = false;
            pbProgress.Visible = false;

            // Restore cursor
            Cursor.Current = _origCursor;

            // Report Errors to User
            int errors = 0;
            if (e.Result is int)
                errors = (int)e.Result;

            if (this._mode == SyncType.Upsert)
                if (errors == 0)
                {
                    Logger.Info(GenesysResources.UploadCompleteNoErrors, "Genesys");
                    MessageBox.Show(GenesysResources.UploadCompleteNoErrors, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Logger.Warning(string.Format(GenesysResources.UploadCompleteErrors, errors), "Genesys");
                    MessageBox.Show(string.Format(GenesysResources.UploadCompleteErrors, errors), this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                if (errors == 0)
                {
                    Logger.Info(GenesysResources.DeleteCompleteNoErrors, "Genesys");
                    MessageBox.Show(GenesysResources.DeleteCompleteNoErrors, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Logger.Warning(string.Format(GenesysResources.DeleteCompleteErrors, errors), "Genesys");
                    MessageBox.Show(string.Format(GenesysResources.DeleteCompleteErrors, errors), this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
        }

        /// <summary>
        /// Cancel button clicked to close the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Reviews all the fields in the source data, identifying matches with passport fields
        /// </summary>
        private void InterrogateData()
        {
            string[] fields = new string[] {
                "INSTCODE", "ACCENUMB", "COLLNUMB","COLLCODE", "COLLNAME",
                "COLLINSTADDRESS", "COLLMISSID", "GENUS","SPECIES",
                "SPAUTHOR", "SUBTAXA", "SUBTAUTHOR", "ACCENAME",
                "ACQDATE","ORIGCTY","COLLSITE","DECLATITUDE","DECLONGITUDE",
                "COORDUNCERT", "COORDDATUM", "GEOREFMETH", "ELEVATION","COLLDATE",
                "BREDCODE", "SAMPSTAT","ANCEST","COLLSRC","DONORCODE",
                "DONORNAME","DONORNUMB","OTHERNUMB", "DUPLSITE",
                "STORAGE", "MLSSTAT", "REMARKS", "ACCEURL", "STATUS_CODE"
            };
            _fieldsToSync.Clear();
            _fieldsMissing.Clear();

            foreach (string field in fields)
            {
                if (_data.Columns.Contains(field))
                    _fieldsToSync.Add(field);
                else
                    _fieldsMissing.Add(field);
            }

            lstFields.DataSource = _fieldsToSync;
            lstMissingFields.DataSource = _fieldsMissing;

            _statusData.Rows.Clear();
            foreach (DataRowView row in _data.DefaultView)
            {
                _statusData.Rows.Add(row["ACCENUMB"].ToString(), string.Empty, row["accession_id"]);
            }

            lblRecordCount.Text = string.Format(GenesysResources.Records, 0, _data.DefaultView.Count);
            lblErrorCount.Text = string.Format(GenesysResources.Errors, 0);
        }

        /// <summary>
        /// Upload Accession Data to Genesys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadAccessionData(object sender, DoWorkEventArgs e)
        {
            // Name the thread for easier debugging
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "Gensys Upload Worker";
            Logger.Info("BEGIN UPLOAD", "Genesys");

            // Clear prior Results
            foreach (DataRow row in _statusData.Rows)
                row[1] = string.Empty;

            // Create Genesys Connection
            IGenesysClient provider = new GenesysClient(_controller.TokenStorage.ServerAddress.AbsoluteUri, _controller.AuthProvider);
            
            
            List<AccessionIdentifier> acceIds = new List<AccessionIdentifier>();
            List<Accession> accessions = new List<Accession>();
            List<AccessionNames> accessionNames = new List<AccessionNames>();
            int rowIdx = 0;
            int errorCount = 0;
            int recordsProcessed = 0;
            int recordsToProcess = _data.DefaultView.Count;
            AccessionIdComparer accessionComparer = new AccessionIdComparer();

            try
            {
                // Process Each Row in the Dataview
                foreach (DataRowView row in _data.DefaultView)
                {
                    // Map from Row to Accession
                    Accession acc = MapAccessionRow(row);

                    // Verify not a duplicate Accession
                    if (acceIds.Contains(acc, accessionComparer))
                    {
                        // Duplicate!
                        _statusData.Rows[rowIdx]["STATUS"] = GenesysResources.Error_Duplicate;
                        Logger.Warning(acc.AcceNumb, string.Format("Upload: {0}", GenesysResources.Error_Duplicate), "Genesys");
                        ++errorCount;
                        ++recordsProcessed;
                        _bw.ReportProgress(100 * recordsProcessed / _data.DefaultView.Count, new UploadStatus() { Processed = recordsProcessed, Errors = errorCount, Total = recordsToProcess });
                    }
                    else
                    {
                        // Not a duplicate, so add to the list to upload
                        acceIds.Add(acc as AccessionIdentifier);
                        accessions.Add(acc);

                        // Map from Row to AccessionNames
                        var names = MapAccessionNamesRow(row);
                        if (names != null)
                            accessionNames.Add(names);
                    }

                    // Upload the batch
                    if (rowIdx + 1 == recordsToProcess || accessions.Count == BATCH_SIZE)
                    {
                        // Upload
                        int resultCount = UploadBatch(provider, accessions, ref errorCount);
                        UploadBatch(provider, accessionNames, ref errorCount);
                        recordsProcessed += resultCount;

                        // Clear batch list
                        accessions.Clear();
                        accessionNames.Clear();

                        _bw.ReportProgress(100 * recordsProcessed / _data.DefaultView.Count, new UploadStatus() { Processed = recordsProcessed, Errors = errorCount, Total = recordsToProcess });
                    }

                    rowIdx++;
                }
            }
            catch (Exception ex)
            {
                Exception currentEx = ex;
                while (currentEx.InnerException != null)
                    currentEx = currentEx.InnerException;

                MessageBox.Show(string.Format(GenesysResources.UnexpectedError, currentEx.Message), GenesysResources.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                e.Result = errorCount;
            }
        }

        /// <summary>
        /// Delete accession data from Genesys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteAccessionData(object sender, DoWorkEventArgs e)
        {
            // Name thread for easier debugging
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "Gensys Delete Worker";
            Logger.Info("BEGIN DELETE", "Genesys");

            // Clear prior Results
            foreach (DataRow row in _statusData.Rows)
                row[1] = string.Empty;

            // Create connection to Genesys
            IGenesysClient provider = new GenesysClient(_controller.TokenStorage.ServerAddress.AbsoluteUri, _controller.AuthProvider);
            
            List<Accession> accessions = new List<Accession>();
            int rowIdx = 0;
            int errorCount = 0;
            int recordsProcessed = 0;
            int recordsToProcess = _data.DefaultView.Count;

            try
            {
                // Collect array of Accessions to Delete
                foreach (DataRowView row in _data.DefaultView)
                {
                    Accession acc = new Accession();

                    // Map required attributes for deletion
                    if (_data.Columns.Contains("INSTCODE") && row["INSTCODE"] != null)
                        acc.InstCode = row["INSTCODE"].ToString();
                    if (_data.Columns.Contains("ACCENUMB") && row["ACCENUMB"] != null)
                        acc.AcceNumb = row["ACCENUMB"].ToString();
                    if (_data.Columns.Contains("GENUS") && row["GENUS"] != null)
                        acc.Genus = row["GENUS"].ToString();

                    // Verify not a duplicate Accession
                    if (accessions.Contains(acc, new AccessionIdComparer()))
                    {
                        _statusData.Rows[rowIdx]["STATUS"] = GenesysResources.Error_Duplicate;
                        Logger.Warning(acc.AcceNumb, string.Format("Delete: {0}", GenesysResources.Error_Duplicate), "Genesys");
                        ++errorCount;
                        ++recordsProcessed;
                        _bw.ReportProgress(100 * recordsProcessed / _data.DefaultView.Count, new UploadStatus() { Processed = recordsProcessed, Errors = errorCount, Total = recordsToProcess });
                    }
                    else
                    {
                        accessions.Add(acc);
                    }

                    // Upload batch
                    if (rowIdx + 1 == recordsToProcess || accessions.Count == BATCH_SIZE)
                    {
                        // Upload
                        int resultCount = UploadBatch(provider, accessions, ref errorCount);
                        recordsProcessed += resultCount;

                        // Clear batch list
                        accessions.Clear();

                        _bw.ReportProgress(100 * recordsProcessed / _data.DefaultView.Count, new UploadStatus() { Processed = recordsProcessed, Errors = errorCount, Total = recordsToProcess });
                    }

                    ++rowIdx;
                }
            }
            catch (Exception ex)
            {
                Exception currentEx = ex;
                while (currentEx.InnerException != null)
                    currentEx = currentEx.InnerException;

                MessageBox.Show(string.Format(GenesysResources.UnexpectedError, currentEx.Message), GenesysResources.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                e.Result = errorCount;
            }
        }

        /// <summary>
        /// Upload a batch of records.  If there is error resulting from the batch command, all the 
        /// accessions in the batch will be marked as having an error.
        /// </summary>
        /// <param name="provider">Genesys connection</param>
        /// <param name="accessions">Batch of accessions</param>
        /// <param name="errorCount">Resulting error count</param>
        /// <returns>Number of accessions processed</returns>
        private int UploadBatch(IGenesysClient provider, IEnumerable<Accession> accessions, ref int errorCount)
        {
            if (accessions == null || accessions.Count() == 0)
                return 0;


            IEnumerable<AccessionOpResponse> results = null;

            try
            {
                if (_mode == SyncType.Delete)
                {
                    // Delete accessions from Genesys
                    results = provider.Accession.Delete(accessions.ElementAt(0).InstCode, accessions.ToArray());
                }
                else
                {
                    // Submit accessions to Genesys
                    results = provider.Accession.Update(accessions.ElementAt(0).InstCode, accessions.ToArray());
                }
            }
            catch (Exception ex)
            {
                // Find the inner exception
                Exception currentEx = ex;
                while (currentEx.InnerException != null)
                    currentEx = currentEx.InnerException;


                // Exception writing the batch of accessions. Log errors for all.
                var batchResults = new List<AccessionOpResponse>();
                foreach (Accession acc in accessions)
                {
                   batchResults.Add(
                        new AccessionOpResponse() 
                        { 
                            AcceNumb = acc.AcceNumb, 
                            Genus = acc.Genus, 
                            InstCode = acc.InstCode, 
                            Error = string.Format("{0}: {1}", GenesysResources.BatchError, currentEx.Message)
                        }
                    ); 
                }
                results = batchResults;
            }

            // Update the status data with the per-accession results
            foreach (AccessionOpResponse result in results)
            {
                var statusRow = _statusData.Select(string.Format("ACCENUMB='{0}' AND STATUS <> '{1}'", result.AcceNumb, GenesysResources.Error_Duplicate));
                if (statusRow.Length > 0)
                    if (result.HasError)
                    {
                        statusRow[0]["STATUS"] = result.Error;
                        Logger.Error(result.AcceNumb, string.Format("Upload: {0}",result.Error), "Genesys");
                        ++errorCount;
                    }
                    else
                    {
                        if (_mode == SyncType.Delete)
                        {
                            statusRow[0]["STATUS"] = GenesysResources.Success;
                            Logger.Info(result.AcceNumb, string.Format("Delete: {0}", GenesysResources.Success), "Genesys");
                        }
                        else
                        {

                            switch (result.Result.Action)
                            {
                                case ActionType.Insert:
                                    statusRow[0]["STATUS"] = GenesysResources.SuccessInsert;
                                    Logger.Info(result.AcceNumb, string.Format("Upload: {0}", GenesysResources.SuccessInsert), "Genesys");
                                    break;
                                case ActionType.Update:
                                    statusRow[0]["STATUS"] = GenesysResources.SuccessUpdate;
                                    Logger.Info(result.AcceNumb, string.Format("Upload: {0}", GenesysResources.SuccessUpdate), "Genesys");
                                    break;
                                case ActionType.Delete:
                                case ActionType.Unknown:
                                default:
                                    statusRow[0]["STATUS"] = GenesysResources.Success;
                                    Logger.Info(result.AcceNumb, string.Format("Delete: {0}", GenesysResources.Success), "Genesys");
                                    break;
                            }
                        }
                    }
            }

            return results.Count();
        }

        /// <summary>
        /// Upload a batch of Accession names
        /// </summary>
        /// <param name="provider">Genesys connection</param>
        /// <param name="accessionNames">Collection of AccessionNames</param>
        /// <param name="errorCount">Error count</param>
        /// <returns>Number of records processed</returns>
        private int UploadBatch(IGenesysClient provider, IEnumerable<AccessionNames> accessionNames, ref int errorCount)
        {
            if (accessionNames == null || accessionNames.Count() == 0)
                return 0;


            IEnumerable<AccessionOpResponse> results = null;

            // Submit accession names to Genesys
            try
            {
                // Submit accessions to Genesys
                results = provider.Accession.UpdateNames(accessionNames.ElementAt(0).InstCode, accessionNames.ToArray());
            }
            catch (Exception ex)
            {
                // Find the inner exception
                Exception currentEx = ex;
                while (currentEx.InnerException != null)
                    currentEx = currentEx.InnerException;


                // Exception writing the batch of accessions. Log errors for all.
                var batchResults = new List<AccessionOpResponse>();
                foreach (AccessionNames acc in accessionNames)
                {
                    batchResults.Add(
                         new AccessionOpResponse()
                         {
                             AcceNumb = acc.AcceNumb,
                             InstCode = acc.InstCode,
                             Error = string.Format("{0}: {1}", GenesysResources.BatchError, currentEx.Message)
                         }
                     );
                }
                results = batchResults;
            }


            // Update the accession status with the results of the call
            foreach (AccessionOpResponse result in results)
            {
                var statusRow = _statusData.Select(string.Format("ACCENUMB='{0}' AND STATUS <> '{1}'", result.AcceNumb, GenesysResources.Error_Duplicate));
                if (statusRow != null && statusRow.Length > 0)
                    if (result.HasError)
                    {
                        // If there is an error, replace a successful entry or append 
                        // to an existing error.
                        string currentStatus = statusRow[0]["STATUS"].ToString();

                        if (string.Compare(currentStatus, GenesysResources.Success, true) == 0 || 
                            string.Compare(currentStatus, GenesysResources.SuccessInsert, true) == 0 || 
                            string.Compare(currentStatus, GenesysResources.SuccessUpdate, true) == 0)
                        {
                            statusRow[0]["STATUS"] = result.Error;
                            ++errorCount;
                            Logger.Error(result.AcceNumb, string.Format("Upload Names: {0}", result.Error), "Genesys");
                        }
                        else
                        {
                            if (string.Compare(currentStatus, result.Error) != 0)
                                statusRow[0]["STATUS"] = string.Format("{0}. {1}", currentStatus, result.Error);
                        }
                    }
            }

            return results.Count();
        }

        /// <summary>
        /// Given a data row, returns an accession that represents the information
        /// </summary>
        /// <param name="row">Row of accession data</param>
        /// <returns>Accession object</returns>
        private Accession MapAccessionRow(DataRowView row)
        {
            Accession acc = new Accession();
            acc.CollectingInformation = new CollectingData();
            acc.Geo = new GISData();

            if (_data.Columns.Contains("INSTCODE") && row["INSTCODE"] != null)
                acc.InstCode = row["INSTCODE"].ToString();
            if (_data.Columns.Contains("ACCENUMB") && row["ACCENUMB"] != null)
                acc.AcceNumb = row["ACCENUMB"].ToString();
            if (_data.Columns.Contains("GENUS") && row["GENUS"] != null)
                acc.Genus = row["GENUS"].ToString();
            if (_data.Columns.Contains("SPECIES") && row["SPECIES"] != null)
                acc.Species = row["SPECIES"].ToString();
            if (_data.Columns.Contains("SPAUTHOR") && row["SPAUTHOR"] != null)
                acc.SPAuthor = row["SPAUTHOR"].ToString();
            if (_data.Columns.Contains("SUBTAXA") && row["SUBTAXA"] != null)
                acc.SubTaxa = row["SUBTAXA"].ToString();
            if (_data.Columns.Contains("SUBTAUTHOR") && row["SUBTAUTHOR"] != null)
                acc.SubtAuthor = row["SUBTAUTHOR"].ToString();
            if (_data.Columns.Contains("ACQDATE") && row["ACQDATE"] != null)
                acc.AcqDate = row["ACQDATE"].ToString();
            if (_data.Columns.Contains("ORIGCTY") && row["ORIGCTY"] != null)
                acc.OrgCty = row["ORIGCTY"].ToString();
            if (_data.Columns.Contains("BREDCODE") && row["BREDCODE"] != null)
                acc.BredCode = row["BREDCODE"].ToString();
            if (_data.Columns.Contains("SAMPSTAT") && row["SAMPSTAT"] != null)
            {
                int val = 0;
                acc.SampStat =
                    int.TryParse(row["SAMPSTAT"].ToString(), out val) ? (int?)val : null;
            }
            if (_data.Columns.Contains("ANCEST") && row["ANCEST"] != null)
                acc.Ancest = row["ANCEST"].ToString();
            if (_data.Columns.Contains("DONORCODE") && row["DONORCODE"] != null)
                acc.DonorCode = row["DONORCODE"].ToString();
            if (_data.Columns.Contains("DONORNAME") && row["DONORNAME"] != null)
                acc.DonorName = row["DONORNAME"].ToString();
            if (_data.Columns.Contains("DONORNUMB") && row["DONORNUMB"] != null)
                acc.DonorNumb = row["DONORNUMB"].ToString();
            if (_data.Columns.Contains("DUPLSITE") && row["DUPLSITE"] != null)
            {
                acc.DuplSite = row["DUPLSITE"].ToString().Split(';');
                for (int i = 0; i < acc.DuplSite.Count(); i++)
                    acc.DuplSite[i] = acc.DuplSite[i].Trim();
            }
            if (_data.Columns.Contains("STORAGE") && row["STORAGE"] != null)
            {
                if (row["STORAGE"] != DBNull.Value)
                {
                    List<int> storageVals = new List<int>();
                    var splitStorage = row["STORAGE"].ToString().Split(';');
                    foreach (string item in splitStorage)
                    {
                        int tempVal = 0;
                        if (int.TryParse(item, out tempVal))
                        {
                            storageVals.Add(tempVal);
                        }
                    }
                    acc.Storage = storageVals.ToArray();
                }
            }
            if (_data.Columns.Contains("MLSSTAT") && row["MLSSTAT"] != null)
            {
                bool val = false;
                int intval = 0;

                // Try to convert from boolean string. If fails, test for 0 or 1
                if (bool.TryParse(row["MLSSTAT"].ToString(), out val))
                    acc.MLSStat = val;
                else if (int.TryParse(row["MLSSTAT"].ToString(), out intval))
                {
                    if (intval == 0)
                        acc.MLSStat = false;
                    else if (intval == 1)
                        acc.MLSStat = true;
                }

            }
            // REMARKS in Genesys is a string array, however GRINGlobal provides a single string in the dataview
            if (_data.Columns.Contains("REMARKS") && row["REMARKS"] != null)
                acc.Remarks = new string[] { row["REMARKS"].ToString() };
            if (_data.Columns.Contains("ACCEURL") && row["ACCEURL"] != null)
                acc.Url = row["ACCEURL"].ToString();

            #region Collecting Information
            if (_data.Columns.Contains("COLLNUMB") && row["COLLNUMB"] != null)
                acc.CollectingInformation.CollNumb = row["COLLNUMB"].ToString();
            if (_data.Columns.Contains("COLLCODE") && row["COLLCODE"] != null)
                acc.CollectingInformation.CollCode = row["COLLCODE"].ToString();
            if (_data.Columns.Contains("COLLNAME") && row["COLLNAME"] != null)
                acc.CollectingInformation.CollName = row["COLLNAME"].ToString();
            if (_data.Columns.Contains("COLLINSTADDRESS") && row["COLLINSTADDRESS"] != null)
                acc.CollectingInformation.CollInstAddress = row["COLLINSTADDRESS"].ToString();
            if (_data.Columns.Contains("COLLMISSID") && row["COLLMISSID"] != null)
                acc.CollectingInformation.CollMissId = row["COLLMISSID"].ToString();
            if (_data.Columns.Contains("COLLSITE") && row["COLLSITE"] != null)
                acc.CollectingInformation.CollSite = row["COLLSITE"].ToString();
            if (_data.Columns.Contains("COLLDATE") && row["COLLDATE"] != null)
                acc.CollectingInformation.CollDate = row["COLLDATE"].ToString();
            if (_data.Columns.Contains("COLLSRC") && row["COLLSRC"] != null)
            {
                int val = 0;

                if (int.TryParse(row["COLLSRC"].ToString(), out val))
                    acc.CollectingInformation.CollSrc = val;
            }
            if (_data.Columns.Contains("STATUS_CODE") && row["STATUS_CODE"] != null)
            {
                acc.Historic = (string.Compare(row["STATUS_CODE"].ToString(), "Historic", true) == 0);
            }
            #endregion

            #region Geo Information
            if (_data.Columns.Contains("DECLATITUDE") && row["DECLATITUDE"] != null)
            {
                double val = 0;
                acc.Geo.Latitude =
                    double.TryParse(row["DECLATITUDE"].ToString(), out val) ? (double?)val : null;
            }
            if (_data.Columns.Contains("DECLONGITUDE") && row["DECLONGITUDE"] != null)
            {
                double val = 0;
                acc.Geo.Longitude =
                    double.TryParse(row["DECLONGITUDE"].ToString(), out val) ? (double?)val : null;
            }
            if (_data.Columns.Contains("COORDUNCERT") && row["COORDUNCERT"] != null)
            {
                double val = 0;
                acc.Geo.CoordUncert =
                    double.TryParse(row["COORDUNCERT"].ToString(), out val) ? (double?)val : null;
            }
            if (_data.Columns.Contains("COORDDATUM") && row["COORDDATUM"] != null)
                acc.Geo.CoordDatum = row["COORDDATUM"].ToString();
            if (_data.Columns.Contains("GEOREFMETH") && row["GEOREFMETH"] != null)
                acc.Geo.GeorefMeth = row["GEOREFMETH"].ToString();
            if (_data.Columns.Contains("ELEVATION") && row["ELEVATION"] != null)
            {
                double val = 0;
                acc.Geo.Elevation =
                    double.TryParse(row["ELEVATION"].ToString(), out val) ? (double?)val : null;
            }
            #endregion

            return acc;
        }

        /// <summary>
        /// Given a data row, returns an AccessionNames object
        /// </summary>
        /// <param name="row">Data row</param>
        /// <returns>AccessionNames object</returns>
        private AccessionNames MapAccessionNamesRow(DataRowView row)
        {
            // Process Accession Name Aliases(ACCENAME, OTHERNUMB)
            if ((_data.Columns.Contains("ACCENAME") && row["ACCENAME"] != null) ||
                (_data.Columns.Contains("OTHERNUMB") && row["OTHERNUMB"] != null))
            {
                List<AccessionAlias> aliases = new List<AccessionAlias>();
                var instCode = row["INSTCODE"].ToString();
                var acceNumb = row["ACCENUMB"].ToString();

                if (_data.Columns.Contains("ACCENAME") && row["ACCENAME"] != null)
                    aliases.Add(new AccessionAlias() { InstCode = instCode, Name = row["ACCENAME"].ToString(), Type = AccessionAliasType.ACCENAME });
                if (_data.Columns.Contains("OTHERNUMB") && row["OTHERNUMB"] != null)
                    aliases.Add(new AccessionAlias() { InstCode = instCode, Name = row["OTHERNUMB"].ToString(), Type = AccessionAliasType.OTHERNUMB });

                return (new AccessionNames() { InstCode = instCode, AcceNumb = acceNumb, Aliases = aliases.ToArray() });
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Helper class providing a mechanism to pass progress info back to the
        /// UI thread.
        /// </summary>
        private class UploadStatus
        {
            /// <summary>
            /// Number of errors
            /// </summary>
            public int Errors { get; set; }

            /// <summary>
            /// Number of records processed
            /// </summary>
            public int Processed { get; set; }

            /// <summary>
            /// Total number of records to process
            /// </summary>
            public int Total { get; set; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRINGlobal.Client.Common
{

    /// <summary>
    /// AppDataTextWriterTraceListener overwrites the tradition Text writer
    /// and ensures the log text file is stored in the AppData directory
    /// for the application. This prevents any rights issues on a protected
    /// machine.
    /// </summary>
    public class AppDataTextWriterTraceListener : TextWriterTraceListener
    {
        public AppDataTextWriterTraceListener(string fileName)
            : base(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\" + System.IO.Path.GetFileName(fileName))
        { }
    }

    /// <summary>
    /// Basic Logging facility backed by the .Net Diagnostics system.
    /// </summary>
    [Switch("SourceSwitch", typeof(SourceSwitch))]
    public static class Logger
    {
        private static TraceSource _traceSource = new TraceSource("GG-Curator");

        /// <summary>
        /// Log an error on an accession
        /// </summary>
        /// <param name="accessionNumber">Accession Number</param>
        /// <param name="message">Message to log</param>
        /// <param name="module">Module where the log event occurred</param>
        public static void Error(string accessionNumber, string message, string module)
        {
            Error(string.Format("{0} - {1}", accessionNumber, message), module);
        }

        /// <summary>
        /// Log an Error
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="module">Module where the log event occurred</param>
        public static void Error(string message, string module)
        {
            WriteEntry(message, TraceEventType.Error, module);
        }

        /// <summary>
        /// Log an exception
        /// </summary>
        /// <param name="ex">Exception to log</param>
        /// <param name="module">Module where the log event occurred</param>
        public static void Error(Exception ex, string module)
        {
            WriteEntry(ex.Message, TraceEventType.Error, module);
        }

        /// <summary>
        /// Log a warning on an accession
        /// </summary>
        /// <param name="accessionNumber">Accession Number</param>
        /// <param name="message">Message to log</param>
        /// <param name="module">Module where the log event occurred</param>
        public static void Warning(string accessionNumber, string message, string module)
        {
            Warning(string.Format("{0} - {1}", accessionNumber, message), module);
        }

        /// <summary>
        /// Log a warning
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="module">Module where the log event occurred</param>
        public static void Warning(string message, string module)
        {
            WriteEntry(message, TraceEventType.Warning, module);
        }

        /// <summary>
        /// Log info on an Accession
        /// </summary>
        /// <param name="accessionNumber">Accession Number</param>
        /// <param name="message">Message to log</param>
        /// <param name="module">Module where the log event occurred</param>
        public static void Info(string accessionNumber, string message, string module)
        {
            Info(string.Format("{0} - {1}", accessionNumber, message), module);
        }

        /// <summary>
        /// Log an info 
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="module">Module where the log event occurred</param>
        public static void Info(string message, string module)
        {
            WriteEntry(message, TraceEventType.Information, module);
        }

        /// <summary>
        /// Method which writed the log information to the Diagnostics trace mechanism of .Net. This 
        /// can be extended through the .Net config file to output the information in different locations.
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="type">ERROR, WARNING, INFO</param>
        /// <param name="module">Module where the error occurred</param>
        private static void WriteEntry(string message, TraceEventType type, string module)
        {
            _traceSource.TraceData(type, 0, string.Format("{0} : {1} : \"{2}\"",
                                  module, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                  message));
        }
    }
}
